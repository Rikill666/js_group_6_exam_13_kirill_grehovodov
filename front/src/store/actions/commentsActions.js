import axiosApi from "../../axiosApi";

export const CREATE_COMMENT_REQUEST = 'CREATE_COMMENT_REQUEST';
export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const CREATE_COMMENT_ERROR = 'CREATE_COMMENT_ERROR';

export const FETCH_COMMENT_REQUEST = 'FETCH_COMMENT_REQUEST';
export const FETCH_COMMENT_SUCCESS = 'FETCH_COMMENT_SUCCESS';
export const FETCH_COMMENT_ERROR = 'FETCH_COMMENT_ERROR';

export const DELETE_COMMENT_REQUEST = 'DELETE_COMMENT_REQUEST';
export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';
export const DELETE_COMMENT_ERROR = 'DELETE_COMMENT_ERROR';

export const deleteCommentRequest = () => {return {type: DELETE_COMMENT_REQUEST};};
export const deleteCommentSuccess = () => ({type: DELETE_COMMENT_SUCCESS});
export const deleteCommentError = (error) => {return {type: DELETE_COMMENT_ERROR, error};};

export const fetchReviewRequest = () => {return {type: FETCH_COMMENT_REQUEST};};
export const fetchReviewSuccess = comment => ({type: FETCH_COMMENT_SUCCESS, comment});
export const fetchReviewError = (error) => {return {type: FETCH_COMMENT_ERROR, error};};

export const createCommentRequest = () => {return {type: CREATE_COMMENT_REQUEST};};
export const createCommentSuccess = () => ({type: CREATE_COMMENT_SUCCESS});
export const createCommentError = (error) => {return {type: CREATE_COMMENT_ERROR, error};};

export const deleteComment = (id) => {
    return async dispatch => {
        try {
            dispatch(deleteCommentRequest());
            await axiosApi.delete('/reviews/' + id);
            dispatch(deleteCommentSuccess());
        } catch (e) {
            dispatch(deleteCommentError(e));
        }
    };
};

export const fetchReview = (shopId) => {
    return async dispatch => {
        try{
            dispatch(fetchReviewRequest());
            const response = await axiosApi.get('/reviews/' + shopId);
            const review = response.data;
            dispatch(fetchReviewSuccess(review));
        }
        catch (e) {
            dispatch(fetchReviewError(e));
        }
    };
};

export const createComment = (commentData) => {
    return async dispatch => {
        try{
            dispatch(createCommentRequest());
            await axiosApi.post('/reviews', commentData);
            dispatch(createCommentSuccess());
        }
        catch (error) {
            if(error.response){
                dispatch(createCommentError(error.response.data));
            }
            else{
                dispatch(createCommentError({global:"Network error or no internet"}));
            }
        }
    };
};