import axiosApi from "../../axiosApi";

export const CREATE_IMAGE_REQUEST = 'CREATE_IMAGE_REQUEST';
export const CREATE_IMAGE_SUCCESS = 'CREATE_IMAGE_SUCCESS';
export const CREATE_IMAGE_ERROR = 'CREATE_IMAGE_ERROR';

export const DELETE_IMAGE_REQUEST = 'DELETE_IMAGE_REQUEST';
export const DELETE_IMAGE_SUCCESS = 'DELETE_IMAGE_SUCCESS';
export const DELETE_IMAGE_ERROR = 'DELETE_IMAGE_ERROR';

export const deleteImageRequest = () => {return {type: DELETE_IMAGE_REQUEST};};
export const deleteImageSuccess = () => ({type: DELETE_IMAGE_SUCCESS});
export const deleteImageError = (error) => {return {type: DELETE_IMAGE_ERROR, error};};

export const createImageRequest = () => {return {type: CREATE_IMAGE_REQUEST};};
export const createImageSuccess = () => ({type: CREATE_IMAGE_SUCCESS});
export const createImageError = (error) => {return {type: CREATE_IMAGE_ERROR, error};};


export const deleteImage = (id) => {
    return async dispatch => {
        try {
            dispatch(deleteImageRequest());
            await axiosApi.delete('/images/' + id);
            dispatch(deleteImageSuccess());
        } catch (e) {
            dispatch(deleteImageError(e));
        }
    };
};

export const createImage = (imageData) => {
    return async dispatch => {
        try{
            dispatch(createImageRequest());
            await axiosApi.post('/images', imageData);
            dispatch(createImageSuccess());
        }
        catch (error) {
            if(error.response){
                dispatch(createImageError(error.response.data));
            }
            else{
                dispatch(createImageError({global:"Network error or no internet"}));
            }
        }
    };
};