import axiosApi from "../../axiosApi";
import {push} from "connected-react-router";

export const FETCH_SHOPS_REQUEST = 'FETCH_SHOPS_REQUEST';
export const FETCH_SHOPS_SUCCESS = 'FETCH_SHOPS_SUCCESS';
export const FETCH_SHOPS_ERROR = 'FETCH_SHOPS_ERROR';

export const FETCH_SHOP_REQUEST = 'FETCH_SHOP_REQUEST';
export const FETCH_SHOP_SUCCESS = 'FETCH_SHOP_SUCCESS';
export const FETCH_SHOP_ERROR = 'FETCH_SHOP_ERROR';

export const CREATE_SHOP_REQUEST = 'CREATE_SHOP_REQUEST';
export const CREATE_SHOP_SUCCESS = 'CREATE_SHOP_SUCCESS';
export const CREATE_SHOP_ERROR = 'CREATE_SHOP_ERROR';

export const DELETE_SHOP_REQUEST = 'DELETE_SHOP_REQUEST';
export const DELETE_SHOP_SUCCESS = 'DELETE_SHOP_SUCCESS';
export const DELETE_SHOP_ERROR = 'DELETE_SHOP_ERROR';

export const fetchShopRequest = () => {return {type: FETCH_SHOP_REQUEST};};
export const fetchShopSuccess = shop => ({type: FETCH_SHOP_SUCCESS, shop});
export const fetchShopError = (error) => {return {type: FETCH_SHOP_ERROR, error};};

export const fetchShopsRequest = () => {return {type: FETCH_SHOPS_REQUEST};};
export const fetchShopsSuccess = shops => ({type: FETCH_SHOPS_SUCCESS, shops});
export const fetchShopsError = (error) => {return {type: FETCH_SHOPS_ERROR, error};};

export const createShopRequest = () => {return {type: CREATE_SHOP_REQUEST};};
export const createShopSuccess = () => ({type: CREATE_SHOP_SUCCESS});
export const createShopError = (error) => {return {type: CREATE_SHOP_ERROR, error};};

export const deleteShopRequest = () => {return {type: DELETE_SHOP_REQUEST};};
export const deleteShopSuccess = () => ({type: DELETE_SHOP_SUCCESS});
export const deleteShopError = (error) => {return {type: DELETE_SHOP_ERROR, error};};

export const fetchShop = (shopId) => {
  return async dispatch => {
    try{
      dispatch(fetchShopRequest());
      const response = await axiosApi.get('/shops/'+ shopId);
      const shop = response.data;
      dispatch(fetchShopSuccess(shop));
    }
    catch (e) {
      dispatch(fetchShopError(e));
    }
  };
};

export const deleteShop = (id) => {
  return async dispatch => {
    try {
      dispatch(deleteShopRequest());
      await axiosApi.delete('/shops/' + id);
      dispatch(deleteShopSuccess());
    } catch (e) {
      dispatch(deleteShopError(e));
    }
  };
};

export const fetchShops = (userId) => {
  let url = '/shops';
  if(userId)
  {
    url+=('?user='+ userId);
  }
  return async dispatch => {
    try{
      dispatch(fetchShopsRequest());
      const response = await axiosApi.get(url);
      const shops = response.data;
      dispatch(fetchShopsSuccess(shops));
    }
    catch (e) {
      dispatch(fetchShopsError(e));
    }
  };
};

export const createShop = (shopData) => {
  return async dispatch => {
    try{
      dispatch(createShopRequest());
      await axiosApi.post('/shops', shopData);
      dispatch(createShopSuccess());
      dispatch(push('/'));
    }
    catch (error) {
      if(error.response){
        dispatch(createShopError(error.response.data));
      }
      else{
        dispatch(createShopError({global:"Network error or no internet"}));
      }
    }
  };
};


