import {
    CREATE_COMMENT_ERROR,
    CREATE_COMMENT_REQUEST,
    CREATE_COMMENT_SUCCESS, FETCH_COMMENT_ERROR,
    FETCH_COMMENT_REQUEST, FETCH_COMMENT_SUCCESS
} from "../actions/commentsActions";

const initialState = {
    comment:null,
    createError:null,
    error:null
};


const commentsReducer = (state = initialState, action) => {
    switch (action.type) {

        case FETCH_COMMENT_REQUEST:
            return {...state};
        case FETCH_COMMENT_SUCCESS:
            return {...state, comment: action.comment, error:null};
        case FETCH_COMMENT_ERROR:
            return {...state, error: action.error};

        case CREATE_COMMENT_REQUEST:
            return {...state, loading: true};
        case CREATE_COMMENT_SUCCESS:
            return {...state, createError: null, loading: false};
        case CREATE_COMMENT_ERROR:
            return {...state, createError: action.error, loading: false};

        default:
            return state;
    }};

export default commentsReducer;