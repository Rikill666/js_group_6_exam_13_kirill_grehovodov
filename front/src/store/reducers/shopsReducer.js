import {
  CREATE_SHOP_ERROR,
  CREATE_SHOP_REQUEST,
  CREATE_SHOP_SUCCESS, FETCH_SHOP_ERROR, FETCH_SHOP_REQUEST, FETCH_SHOP_SUCCESS,
  FETCH_SHOPS_ERROR,
  FETCH_SHOPS_REQUEST,
  FETCH_SHOPS_SUCCESS
} from "../actions/shopsActions";

const initialState = {
  shops: [],
  error: null,
  createError:null,
  loading: false,
  shop:null
};

const shopsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SHOPS_REQUEST:
      return {...state, loading: true};
    case FETCH_SHOPS_SUCCESS:
      return {...state, shops: action.shops, error:null, loading: false};
    case FETCH_SHOPS_ERROR:
      return {...state, error: action.error, loading: false};

    case FETCH_SHOP_REQUEST:
      return {...state, loading: true};
    case FETCH_SHOP_SUCCESS:
      return {...state, shop: action.shop, error: null, loading: false};
    case FETCH_SHOP_ERROR:
      return {...state, error: action.error, loading: false};

    case CREATE_SHOP_REQUEST:
      return {...state, loading: true};
    case CREATE_SHOP_SUCCESS:
      return {...state, createError:null, loading: false};
    case CREATE_SHOP_ERROR:
      return {...state, createError: action.error, loading: false};

    default:
      return state;
  }
};

export default shopsReducer;