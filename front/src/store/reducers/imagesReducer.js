import {CREATE_IMAGE_ERROR, CREATE_IMAGE_REQUEST, CREATE_IMAGE_SUCCESS} from "../actions/imagesActions";

const initialState = {
    image:null,
    createError:null,
};


const imagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_IMAGE_REQUEST:
            return {...state, loading: true};
        case CREATE_IMAGE_SUCCESS:
            return {...state, createError: null, loading: false};
        case CREATE_IMAGE_ERROR:
            return {...state, createError: action.error, loading: false};

        default:
            return state;
    }};

export default imagesReducer;