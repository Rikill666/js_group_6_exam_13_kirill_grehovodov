import React from 'react';
import {useSelector} from "react-redux";
import {Redirect, Route, Switch} from "react-router-dom";
import Shops from "./containers/Shops/Shops";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import NewShop from "./containers/NewShop/NewShop";
import UserProfile from "./containers/UserProfile/UserProfile";
import FullShopDetails from "./containers/FullShopDetails/FullShopDetails";

const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props}/> : <Redirect to="/login"/>
);

const Routes = () => {
  const user = useSelector(state => state.users.user);

  return (
    <Switch>
      <Route path="/" exact component={Shops} />
      <Route path="/shops" exact component={Shops} />
      <Route path="/register" exact component={Register} />
      <Route path="/login" exact component={Login} />
      <ProtectedRoute isAllowed={user} path="/profile" exact component={UserProfile} />
      <ProtectedRoute isAllowed={user} path="/shops/new" exact component={NewShop} />
      <Route path="/shops/:id" exact component={FullShopDetails} />
    </Switch>
  );
};

export default Routes;