import React, {Component} from 'react';
import ShopForm from "../../components/ShopForm/ShopForm";
import {createShop} from "../../store/actions/shopsActions";
import {connect} from "react-redux";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Toolbar from "@material-ui/core/Toolbar";

class NewShop extends Component {

    shopCreate = async (shopData) => {
        await this.props.createShop(shopData);
    };

    render() {
        return (
            <div style={{width:"50%", marginLeft:"25%"}}>
                <Toolbar/>
                <Box pb={2} pt={2}>
                    <Typography variant="h4">Add new shop</Typography>
                </Box>

                <ShopForm
                    error = {this.props.createError}
                    onSubmit={this.shopCreate}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    createError: state.shops.createError
});

const mapDispatchToProps = dispatch => ({
    createShop: shopData => dispatch(createShop(shopData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewShop);