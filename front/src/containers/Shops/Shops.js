import React, {Component} from 'react';
import {fetchShops} from "../../store/actions/shopsActions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import ShopListItem from "../../components/ShopListItem/ShopListItem";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import 'react-image-lightbox/style.css';
import Toolbar from "@material-ui/core/Toolbar";


class Shops extends Component {

    async componentDidMount() {
        if (this.props.match.params.id) {
            await this.props.fetchShops(this.props.match.params.id);
        } else {
            await this.props.fetchShops();
        }
    }

    render() {
        return (

            <Grid container direction="column" spacing={2}>
                <Toolbar/>
                <Grid item container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                        {
                            this.props.user ?
                                <Button
                                    color="primary"
                                    component={Link}
                                    to={"/shops/new"}
                                >
                                    <Typography variant="h6">
                                        Add new shop
                                    </Typography>
                                </Button> : null
                        }
                    </Grid>
                </Grid>
                <Grid item container direction="row" spacing={2} justify="center">
                    {this.props.shops.map(shop => {
                        return <ShopListItem
                            key={shop._id}
                            title={shop.title}
                            id={shop._id}
                            image={shop.image}
                            description={shop.description}
                            rating={shop.averageRating}
                            imagesCount={shop.imagesCount}
                            reviewsCount={shop.reviewsCount}
                        />
                    })}
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => ({
    shops: state.shops.shops,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    fetchShops: (userId) => dispatch(fetchShops(userId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Shops);
