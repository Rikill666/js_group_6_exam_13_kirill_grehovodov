import React, {Component} from 'react';
import {connect} from "react-redux";
import ShopInfo from "../../components/ShopInfo/ShopInfo";
import Comment from "../../components/ShopInfo/Comment";
import Toolbar from "@material-ui/core/Toolbar";
import {fetchShop} from "../../store/actions/shopsActions";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import PopUp from "../../components/PopUp/PopUp";
import Typography from "@material-ui/core/Typography";
import CommentForm from "../../components/CommentForm/CommentForm";
import {createComment, deleteComment, fetchReview} from "../../store/actions/commentsActions";
import ImageForm from "../../components/ImageForm/ImageForm";
import {createImage, deleteImage} from "../../store/actions/imagesActions";
import config from "../../config";
import Button from "@material-ui/core/Button";
import CircularProgress from '@material-ui/core/CircularProgress';
import ShowTo from "../../hoc/ShowTo";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';


class FullShopDetails extends Component {
    state = {
        isOpen: false,
        imageId: ""
    };


    async componentDidMount() {
        await this.props.fetchShop(this.props.match.params.id);
        await this.props.fetchReview(this.props.match.params.id);
    }

    openPopUp = (id) => {
        this.setState({isOpen: true, imageId: id});
    };
    closePopUp = () => {
        this.setState({isOpen: false});
    };

    createComment = async (commentForm) => {
        await this.props.createComment(commentForm);
        if (!this.props.createCommentError) {
            await this.props.fetchReview(this.props.match.params.id);
            await this.props.fetchShop(this.props.match.params.id)
        }

    };

    deleteComment = async (id) => {
        await this.props.deleteComment(id);
        await this.props.fetchReview(this.props.match.params.id);
        await this.props.fetchShop(this.props.match.params.id)

    };
    deleteImage = async (id) => {
        await this.props.deleteImage(id);
        await this.props.fetchShop(this.props.match.params.id)
    };

    createImage = async (imageForm) => {
        await this.props.createImage(imageForm);
        await this.props.fetchShop(this.props.match.params.id)
    };

    render() {
        return (
            <>
                <Toolbar/>
                {this.props.shop && !this.props.loading ?
                    <>
                        <ShopInfo
                            image={this.props.shop.image}
                            title={this.props.shop.title}
                            averageRating={this.props.shop.averageRating}
                            averageKitchenQuality={this.props.shop.averageKitchenQuality}
                            averageServiceQuality={this.props.shop.averageServiceQuality}
                            averageInteriorQuality={this.props.shop.averageInteriorQuality}
                            description={this.props.shop.description}
                            id={this.props.shop._id}
                        />
                        {this.props.shop.images.length > 0 ?
                            <Grid style={{marginBottom: "25px"}} container direction="row" spacing={2}>
                                {this.props.shop.images.map(image => {
                                    return <Grid item xs={6} sm={3} md={2} key={image._id}>
                                        <ShowTo role={'admin'}>
                                            <HighlightOffIcon style={{position:'relative', left:'80%',top:'15%', cursor:"pointer"}} onClick={() => this.deleteImage(image._id)}/>
                                        </ShowTo>
                                        <CardMedia
                                            onClick={() => this.openPopUp(image._id)}
                                            style={{width: '100%', height: "100%"}}
                                            component="img"
                                            id={image._id}
                                            height="140"
                                            image={config.apiURL + "/" + image.url}
                                            title="image"
                                        />

                                        {this.state.isOpen && this.state.imageId === image._id ?
                                            <PopUp image={"http://localhost:8000/" + image.url}
                                                   isOpen={this.state.isOpen}
                                                   close={this.closePopUp}
                                            /> : ""}
                                    </Grid>
                                })}
                            </Grid> : <p>No photo</p>}
                        {this.props.shop.reviews.length > 0 ?
                            <div>
                                <Typography variant="h6">
                                    Reviews
                                </Typography>
                                {this.props.shop.reviews.map(review => {
                                    return <Comment key={review._id}
                                                    online_user = {this.props.user}
                                                    author_id = {review.user._id}
                                                    author={review.user.displayName}
                                                    comment={review.comment}
                                                    createDate={review.createDate}
                                                    id={review._id}
                                                    kitchenQuality={review.kitchenQuality}
                                                    interiorQuality={review.interiorQuality}
                                                    serviceQuality={review.serviceQuality}
                                                    deleteClick={this.deleteComment}
                                    />
                                })}
                            </div>
                            : <p>No comments</p>}
                        {this.props.user ?
                            <div>
                                {!this.props.review ? <div>
                                    <Typography style={{margin: "20px 0 10px"}} variant="h6">
                                        Add review
                                    </Typography>
                                    <CommentForm shopId={this.props.shop._id}
                                                 error={this.props.createCommentError}
                                                 createComment={(comment) => this.createComment(comment)}
                                    />
                                </div> : null}
                                <Typography style={{margin: "20px 0 10px"}} variant="h6">
                                    Upload new photo
                                </Typography>
                                <ImageForm shopId={this.props.shop._id}
                                           error={this.props.createImageError}
                                           createImage={(image) => this.createImage(image)}
                                />
                            </div> : null}
                    </> : <div style={{"display":"flex"}}>
                        <CircularProgress color="secondary" size={300} style={{margin:"10% auto 0"}}/>
                    </div>}
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        shop: state.shops.shop,
        user: state.users.user,
        createCommentError: state.comments.createError,
        createImageError: state.images.createError,
        review: state.comments.comment,
        loading: state.shops.loading,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchShop: (shopId) => dispatch(fetchShop(shopId)),
        createComment: (commentForm) => dispatch(createComment(commentForm)),
        createImage: (imageForm) => dispatch(createImage(imageForm)),
        fetchReview: (shopId) => dispatch(fetchReview(shopId)),
        deleteComment: (reviewId) => dispatch(deleteComment(reviewId)),
        deleteImage: (imageId) => dispatch(deleteImage(imageId)),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(FullShopDetails);