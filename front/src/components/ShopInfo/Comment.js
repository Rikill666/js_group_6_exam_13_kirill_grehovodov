import React from 'react';
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Rating from "@material-ui/lab/Rating";
import {makeStyles} from "@material-ui/core/styles";
import Moment from "react-moment";
import ShowTo from "../../hoc/ShowTo";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({

    content: {
        display: 'flex',
        flexDirection: 'column',
        flex: '1 0 auto',
        height: "100%",
        justifyContent: 'space-between'
    },
    title: {
        marginBottom: "20px"
    },
    rating: {
        alignSelf: "center",
        display: 'flex',
        alignItems: 'center',
    },
    ratingTitle: {
        fontSize: '20px',
        whiteSpace: 'pre'
    },
    ratingNumber: {
        fontSize: '20px',
        marginLeft: '25px'
    },
    description: {
        fontSize: '18px'
    },
    stars: {
        alignSelf: "center",
    }
});

const Comment = (props) => {
    const classes = useStyles();
    return (
        <div style={{borderBottom: "1px solid black", marginBottom: "25px"}}>
            <Typography variant="subtitle1">
                On <Moment format="YYYY/MM/DD">{props.createDate}</Moment>, {props.author}
            </Typography>
            <Typography variant="body1">
                {props.comment}
            </Typography>
            <div>
                <div className={classes.rating}>
                    <Box className={classes.ratingTitle}>Quality of food:{" ".repeat(6)}</Box>
                    <Rating className={classes.stars} value={props.kitchenQuality} size="large" precision={0.2}
                            readOnly/>
                    <Box className={classes.ratingNumber}>{props.kitchenQuality}</Box>
                </div>
                <div className={classes.rating}>
                    <Box className={classes.ratingTitle}>Service quality:{" ".repeat(6)}</Box>
                    <Rating value={props.serviceQuality} size="large" precision={0.2} readOnly/>
                    <Box className={classes.ratingNumber}>{props.serviceQuality}</Box>
                </div>
                <div className={classes.rating}>
                    <Box className={classes.ratingTitle}>Interior:{" ".repeat(19)}</Box>
                    <Rating value={props.interiorQuality} size="large" precision={0.2} readOnly/>
                    <Box className={classes.ratingNumber}>{props.interiorQuality}</Box>
                </div>
            </div>
            {props.online_user?
                props.online_user._id === props.author_id || props.online_user.role==="admin"?
                <Button onClick={() => props.deleteClick(props.id)} variant="contained" color="primary">
                    Delete
                </Button>:null
            :null}
        </div>
    );
};
Comment.propTypes = {
    author: PropTypes.string,
    id: PropTypes.string.isRequired,
    comment: PropTypes.string.isRequired,
    kitchenQuality: PropTypes.number.isRequired,
    serviceQuality: PropTypes.number.isRequired,
    interiorQuality: PropTypes.number.isRequired,
    createDate: PropTypes.string.isRequired,
};
export default Comment;