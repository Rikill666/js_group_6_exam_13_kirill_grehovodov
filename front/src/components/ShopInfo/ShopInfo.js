import React from 'react';
import CardContent from "@material-ui/core/CardContent";
import PropTypes from 'prop-types';
import Typography from "@material-ui/core/Typography";
import {Card} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import config from "../../config";
import imageNotAvailable from "../../assets/images/image_not_available.jpg";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Rating from "@material-ui/lab/Rating";
import {EditorState, convertFromRaw} from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';

const useStyles = makeStyles({
    image: {
        width: "100%"
    },
    content: {
        display: 'flex',
        flexDirection: 'column',
        flex: '1 0 auto',
        height: "100%",
        justifyContent: 'space-between'
    },
    title: {
        marginBottom: "20px"
    },
    rating: {
        alignSelf: "center",
        display: 'flex',
        alignItems: 'center',
    },
    ratingTitle: {
        fontSize: '20px',
        whiteSpace: 'pre'
    },
    ratingNumber:{
        fontSize: '20px',
        marginLeft:'25px'
    },
    description: {
        fontSize: '18px'
    },
    stars:{
        alignSelf: "center",
    }
});


const ShopInfo = (props) => {
    let image = imageNotAvailable;
    if (props.image) {
        image = config.apiURL + '/' + props.image.replace('\\', '/');
    }
    const classes = useStyles();
    const getDescription = () => {
        try {
            const description = EditorState.createWithContent(convertFromRaw(JSON.parse(props.description)));
            return <Editor readOnly toolbarHidden editorState={description}/>;
        } catch (e) {
            return "No description available";
        }
    };
    return <Card>
        <Grid container direction="row">
            <Grid item xs={12} md={6} lg={6}>
                <CardContent className={classes.content}>
                    <div>
                        <Typography className={classes.title} variant="h3">
                            {props.title}
                        </Typography>
                            {getDescription()}
                    </div>
                    <div>
                        <Typography variant="h6">
                            Ratings
                        </Typography>
                        <div className={classes.rating}>
                            <Box className={classes.ratingTitle}>Overall:{" ".repeat(20)}</Box>
                            <Rating value={props.averageRating} size="large" precision={0.2} readOnly/>
                            <Box className={classes.ratingNumber}>{props.averageRating}</Box>
                        </div>
                        <div className={classes.rating}>
                            <Box className={classes.ratingTitle}>Quality of food:{" ".repeat(6)}</Box>
                            <Rating className={classes.stars} value={props.averageKitchenQuality} size="large" precision={0.2} readOnly/>
                            <Box className={classes.ratingNumber}>{props.averageKitchenQuality}</Box>
                        </div>
                        <div className={classes.rating}>
                            <Box className={classes.ratingTitle}>Service quality:{" ".repeat(6)}</Box>
                            <Rating value={props.averageServiceQuality} size="large" precision={0.2} readOnly/>
                            <Box className={classes.ratingNumber}>{props.averageServiceQuality}</Box>
                        </div>
                        <div className={classes.rating}>
                            <Box className={classes.ratingTitle}>Interior:{" ".repeat(19)}</Box>
                            <Rating value={props.averageInteriorQuality} size="large" precision={0.2} readOnly/>
                            <Box className={classes.ratingNumber}>{props.averageInteriorQuality}</Box>
                        </div>
                    </div>
                </CardContent>
            </Grid>
            <Grid item xs={12} md={6} lg={6}>
                <img className={classes.image} src={image} alt={props.title}/>
            </Grid>
        </Grid>
    </Card>
};
ShopInfo.propTypes = {
    image: PropTypes.string,
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    averageRating: PropTypes.number.isRequired,
    averageKitchenQuality: PropTypes.number.isRequired,
    averageServiceQuality: PropTypes.number.isRequired,
    averageInteriorQuality: PropTypes.number.isRequired,
    description: PropTypes.string.isRequired,
};

export default ShopInfo;