import React from 'react';
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import {Card} from "@material-ui/core";
import CardMedia from "@material-ui/core/CardMedia";
import {makeStyles} from "@material-ui/core/styles";
import config from "../../config";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import {useDispatch, useSelector} from "react-redux";
import {deleteShop, fetchShops} from "../../store/actions/shopsActions";
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import Rating from '@material-ui/lab/Rating';
import {Link} from "react-router-dom";
import ShowTo from "../../hoc/ShowTo";
import CloseIcon from '@material-ui/icons/Close';
import CardHeader from "@material-ui/core/CardHeader";
import IconButton from "@material-ui/core/IconButton";

const useStyles = makeStyles({
    card: {
        width: "70%",
        marginLeft: "15%"
    },
    media: {
        paddingTop: '100%', // 16:9
    },
    cardShell: {
        textAlign: 'center',
    },
    link: {
        textDecoration: 'none',
        color: "black"
    },
    camera: {
        verticalAlign: 'middle'
    },
    header: {
        display: 'block',
    },
});

const ShopListItem = props => {
    const user = useSelector(state => state.users.user);
    //const [isOpen, setIsOpen] = useState(false);
    const dispatch = useDispatch();

    const classes = useStyles();
    const image = config.apiURL + '/' + props.image.replace('\\', '/');

    const cardDelete = async (id) => {
        await dispatch(deleteShop(id));
        await dispatch(fetchShops());
    };

    return (
        <Grid item xs={12} sm={6} md={4}>
            <Card className={classes.card}>
                <ShowTo>
                    <CardHeader className={classes.header}
                                action={
                                    <div style={{
                                        display: "flex",
                                        justifyContent: "space-between"
                                    }}>
                                        <IconButton
                                            onClick={() => cardDelete(props.id)}
                                            aria-label="settings">
                                            <CloseIcon color={'action'}
                                                       fontSize={'large'}/>
                                        </IconButton>
                                    </div>
                                }
                    />
                </ShowTo>
                <Link to={"/shops/" + props.id} className={classes.link}>
                    <CardMedia image={image} title={props.title} className={classes.media}/>
                    <CardContent>
                        <Typography variant="h3">
                            {props.title}
                        </Typography>
                        <Typography variant="h3">
                            <Rating value={props.rating} size="large" precision={0.2} readOnly/>
                        </Typography>
                        <Typography variant="h5">
                            ({props.rating}, {props.reviewsCount} reviews)
                        </Typography>
                        <Typography variant="h5">
                            <PhotoCameraIcon className={classes.camera}/> {props.imagesCount} photos
                        </Typography>
                    </CardContent>
                </Link>
                <CardActions>
                    {user && user.role === "admin" ?
                        <Button onClick={() => cardDelete(props.id)} variant="contained" color="primary">
                            Delete
                        </Button> : null
                    }
                </CardActions>
            </Card>
        </Grid>
    );
};

ShopListItem.propTypes = {
    image: PropTypes.string,
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
};

export default ShopListItem;