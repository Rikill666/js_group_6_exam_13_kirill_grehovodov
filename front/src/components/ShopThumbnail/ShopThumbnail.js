import React from 'react';

import imageNotAvailable from '../../assets/images/image_not_available.jpg';
import {apiURL} from "../../config";

const styles = {
  width: '100px',
  height: '100px',
  marginRight: '10px'
};

const ShopThumbnail = props => {
  let image = imageNotAvailable;

  if (props.image) {
    image = apiURL + '/' + props.image;
  }

  return <img alt="product" src={image} style={styles} className="img-thumbnail" />;
};

export default ShopThumbnail;