import React from 'react';
import PropTypes from 'prop-types';
import {Editor} from "react-draft-wysiwyg";
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import FileInput from "./FileInput";
import {makeStyles} from "@material-ui/core/styles";
import Radio from '@material-ui/core/Radio';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
    wysiwyg: {
        border: '1px solid rgba(0, 0, 0, 0.23)',
        borderRadius: theme.shape.borderRadius
    },
    consentBlock: {
        color: "red"
    },
    wysiwygError: {
        color: "red",
        marginTop: "0px",
        marginLeft: "14px",
        fontSize: "0.75rem",
        align: "left"
    }
}));

const FormElement = props => {
    const classes = useStyles();
    let inputChildren = undefined;

    if (props.type === 'select') {
        inputChildren = props.options.map(o => (
            <MenuItem key={o.id} value={o.id}>
                {o.title}
            </MenuItem>
        ));
    }

    let inputComponent = (
        <TextField
            fullWidth
            variant="outlined"
            label={props.title}
            error={!!props.error}
            type={props.type}
            select={props.type === 'select'}
            name={props.propertyName}
            id={props.propertyName}
            value={props.value}
            onChange={props.onChange}
            required={props.required}
            autoComplete={props.autoComplete}
            placeholder={props.placeholder}
            children={inputChildren}
            helperText={props.error}
        >
            {inputChildren}
        </TextField>
    );
    if (props.type === 'consent') {
        inputComponent = (
            <Grid container direction="row" spacing={2}>
                <Grid item xs>
                    <Typography component="div">
                        <Box textAlign="justify" m={1}>
                            You agree to the terms
                        </Box>
                    </Typography>
                </Grid>
                <Grid item xs>
                    <FormControlLabel
                        control={
                            <Radio
                                onChange={props.onChange}
                                value={props.value}
                            />
                        } label={props.title}/>
                    <p className={classes.wysiwygError}>
                        {props.error}
                    </p>
                </Grid>
            </Grid>
        )
    }
    if (props.type === 'wysiwyg') {
        inputComponent = (
            <Grid container direction="column" spacing={2}>
                <Grid item xs>
                    <Editor
                        contentState={null}
                        onContentStateChange={props.onChange}
                        editorClassName={classes.wysiwyg}
                    />
                    <p className={classes.wysiwygError}>{props.error}</p>
                </Grid>
            </Grid>
        );
    }
    if (props.type === 'file') {
        inputComponent = (
            <FileInput
                error={!!props.error}
                helperText={props.error}
                label={props.title}
                name={props.propertyName}
                onChange={props.onChange}
            />
        )
    }

    return inputComponent;
};

FormElement.propTypes = {
    propertyName: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    required: PropTypes.bool,
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool, PropTypes.array]),
    options: PropTypes.arrayOf(PropTypes.object),
    onChange: PropTypes.func.isRequired,
    error: PropTypes.string,
    autoComplete: PropTypes.string,
};

export default FormElement;