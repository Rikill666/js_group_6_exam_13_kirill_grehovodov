import React, {Component} from 'react';
import FormElement from "../UI/Form/FormElement";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Rating from "@material-ui/lab/Rating";
import Typography from "@material-ui/core/Typography";


class CommentForm extends Component {
    state = {
        comment: '',
        kitchenQuality: 0,
        serviceQuality: 0,
        interiorQuality: 0
    };

    submitFormHandler = async event => {
        event.preventDefault();
        let comment = {...this.state, shop:this.props.shopId};
        await this.props.createComment(comment);
        if(!this.props.error){
            this.setState({comment:"", kitchenQuality:0, serviceQuality:0, interiorQuality:0});
        }
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    ratingChangeHandler = (event) => {
        this.setState({[event.target.name]: parseInt(event.target.value)});
    };

    getFieldError = fieldName => {
        try {
            return this.props.error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    render() {
        return (
            <form onSubmit={this.submitFormHandler}>
                <Grid container direction="column" spacing={2}>
                    <Grid item xs>
                        <FormElement
                            type="textarea"
                            title="Comment"
                            propertyName="comment"
                            placeholder="Enter comment"
                            onChange={this.inputChangeHandler}
                            value={this.state.comment}
                            error={this.getFieldError('comment')}
                        />
                    </Grid>
                    <Grid item xs container direction="row" justify='space-around'>
                        <Grid item xs={12} sm={6} md={4} style={{textAlign:'center'}}>
                            <Typography component="legend">Quality of food</Typography>
                            <Rating
                                name="kitchenQuality"
                                value={this.state.kitchenQuality}
                                onChange={this.ratingChangeHandler}
                            />
                            <div style={{color:'red', fontSize:'12px'}}>
                               {this.getFieldError('kitchenQuality')}
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={6} md={4} style={{textAlign:'center'}}>
                            <Typography component="legend">Service quality</Typography>
                            <Rating
                                name="serviceQuality"
                                value={this.state.serviceQuality}
                                onChange={this.ratingChangeHandler}
                            />
                            <div style={{color:'red', fontSize:'12px'}}>
                                {this.getFieldError('serviceQuality')}
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={6} md={4} style={{textAlign:'center'}}>
                            <Typography component="legend">Interior</Typography>
                            <Rating
                                name="interiorQuality"
                                value={this.state.interiorQuality}
                                onChange={this.ratingChangeHandler}
                            />
                            <div style={{color:'red', fontSize:'12px'}}>
                                {this.getFieldError('interiorQuality')}
                            </div>
                        </Grid>
                    </Grid>
                    <Grid item xs style={{textAlign:"right"}}>
                        <Button type="submit" color="primary" variant="contained">Submit review</Button>
                    </Grid>
                </Grid>
            </form>
        );
    }
}

export default CommentForm;