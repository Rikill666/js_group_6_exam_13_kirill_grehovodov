import React from 'react';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import CloseIcon from '@material-ui/icons/Close';

const PopUp = (props) => {
    return (
        <div>
            {props.isOpen && (
                <Lightbox
                    mainSrc={props.image}
                    onCloseRequest={() => props.close()}
                    closeLabel={"Close"}
                >
                    <CloseIcon/>
                </Lightbox>
            )}
        </div>
    );
};

export default PopUp;