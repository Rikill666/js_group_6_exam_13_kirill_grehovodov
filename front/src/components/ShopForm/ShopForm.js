import React, {Component} from 'react';
import FormElement from "../UI/Form/FormElement";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";


class ShopForm extends Component {
    state = {
        title: '',
        image: '',
        description: null,
        consent: false
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            let value = this.state[key];
            if (key === 'description') {
                value = JSON.stringify(value);
            }
            formData.append(key, value);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    consentChangeHandler = event => {
        this.setState({consent: !this.state.consent});
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    getFieldError = fieldName => {
        try {
            return this.props.error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };
    editorChangeHandler = description => {
        this.setState({description});
    };

    render() {
        return (
            <form onSubmit={this.submitFormHandler}>
                <Grid container direction="column" spacing={2}>
                    <Grid item xs>
                        <FormElement
                            type="title"
                            propertyName="title"
                            title="Title"
                            placeholder="Enter title"
                            onChange={this.inputChangeHandler}
                            value={this.state.title}
                            error={this.getFieldError('title')}
                        />
                    </Grid>
                    <Grid item xs>
                        <FormElement
                            type="wysiwyg"
                            propertyName="description"
                            title="Description"
                            onChange={this.editorChangeHandler}
                            error={this.getFieldError('description')}
                        />
                    </Grid>
                    <Grid item xs>
                        <FormElement
                            type="file"
                            propertyName="image"
                            title="Image"
                            onChange={this.fileChangeHandler}
                            error={this.getFieldError('image')}
                        />
                    </Grid>
                    <Grid item xs>
                        <FormElement
                            type="consent"
                            propertyName="consent"
                            value={this.state.consent}
                            title="I agree"
                            onChange={this.consentChangeHandler}
                            error={this.getFieldError('consent')}
                        />
                    </Grid>
                    <Grid item xs>
                        <Button type="submit" color="primary" variant="contained">Create shop</Button>
                    </Grid>
                </Grid>
            </form>
        );
    }
}

export default ShopForm;