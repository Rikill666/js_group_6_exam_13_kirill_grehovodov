const env = process.env.REACT_APP_ENV;

let apiURL = 'http://localhost:8000';
const facebookAppId = "622346031722687";

if (env === 'test') {
    apiURL = 'http://localhost:8010';
}

const config = {
    apiURL,
    facebookAppId,
};

export default config;