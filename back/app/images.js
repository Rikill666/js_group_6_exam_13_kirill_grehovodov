const express = require('express');
const ValidationError = require('mongoose').Error.ValidationError;
const auth = require('../middleware/auth');
const upload = require('../multer').uploads;
const Image = require('../models/Image');
const Shop = require('../models/Shop');
const permit = require('../middleware/permit');
const router = express.Router();

router.get('/:id', async (req, res) => {
    const id = req.params.id;
    if (id) {
        try {
            const images = await Image.find({shop: id});
            return res.send(images);
        } catch (error) {
            return res.status(400).send(error);
        }
    } else {
        return res.status(404).send({message: "Shop not found"});
    }
});

router.post('/', [auth, upload.single('url')], async (req, res) => {
    if (!req.file) {
        return res.status(400).send({errors: {url: {message: "You have not photo"}}});
    }
    try {
        const user = req.user;
        const shop = await Shop.findOne({_id: req.body.shop});
        if (!shop) {
            return res.status(404).send({message: "Shop not found"});
        }
        const imageData = {
            shop: shop,
            user: user,
            url: req.file.filename
        };

        const image = new Image(imageData);
        await image.save();
        return res.send(image);
    } catch (e) {
        if (e instanceof ValidationError) {
            return res.status(400).send(e);
        } else {
            console.log(e);
            return res.sendStatus(500);
        }
    }
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const id = req.params.id;
    try {
        const image = await Image.findOne({_id: id});
        if (!image) {
            return res.status(404).send("No this image");
        }
        await Image.deleteOne({_id: id});
        res.send("ok");
    } catch (error) {
        return res.status(400).send(error);
    }
});
module.exports = router;