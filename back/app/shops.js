const express = require('express');
const ValidationError = require('mongoose').Error.ValidationError;
const auth = require('../middleware/auth');
const upload = require('../multer').uploads;
const Shop = require('../models/Shop');
const permit = require('../middleware/permit');
const router = express.Router();
const Review = require('../models/Review');
const Image = require('../models/Image');
const fs = require('fs');
const config = require('../config');
const path = require('path');

router.get('/', async (req, res) => {
    let shops;
    if (req.query.user) {
        shops = await Shop.find({user: req.query.user}).populate("user", "username displayName _id");
    } else {
        shops = await Shop.find().populate("user", "username displayName");
    }
    shops = await Promise.all(shops.map(async shop => {

        let imagesCount = await Image.aggregate([{$match: {shop: shop._id}},
            {$count: "count"}]);
        imagesCount = imagesCount.length > 0 ? imagesCount[0].count : 0;
        let reviewsCount = await Review.aggregate([{$match: {shop: shop._id}}, {
            $count: "count"
        }]);
        reviewsCount = reviewsCount.length > 0 ? reviewsCount[0].count : 0;
        return {...shop._doc, imagesCount: imagesCount, reviewsCount: reviewsCount};
    }));
    res.send(shops);
});


router.post('/', [auth, upload.single('image'),], async (req, res) => {

    if (!req.body.title) {
        if (req.file) {
            await fs.promises.unlink(path.join(config.uploadPath, req.file.filename));
        }
        return res.status(400).send({errors: {title: {message: "Fill in this field"}}});
    }
    if (!JSON.parse(req.body.description)) {
        if (req.file) {
            await fs.promises.unlink(path.join(config.uploadPath, req.file.filename));
        }
        return res.status(400).send({errors: {description: {message: "Fill in this field"}}});
    }
    if (!req.file) {
        return res.status(400).send({errors: {image: {message: "Fill in this field"}}});
    }
    if (!JSON.parse(req.body.consent)) {
        if (req.file) {
            await fs.promises.unlink(path.join(config.uploadPath, req.file.filename));
        }
        return res.status(400).send({errors: {consent: {message: "You have not ticked the agreement"}}});
    }

    try {
        const user = req.user;
        const shopData = {
            user: user,
            title: req.body.title,
            description: req.body.description,
            image: req.file.filename
        };

        const shop = new Shop(shopData);
        await shop.save();
        return res.send({id: shop._id});
    } catch (e) {
        if (e instanceof ValidationError) {
            return res.status(400).send(e);
        } else {
            console.log(e);
            return res.sendStatus(500);
        }
    }
});

router.get("/:id", async (req, res) => {
    const id = req.params.id;
    if (id) {
        try {
            const shop = await Shop.findOne({_id: id});
            if (!shop) {
                return res.status(404).send({message: "Shop not found"});
            }

            const reviews = await Review.find({shop: id}).populate('user');
            const images = await Image.find({shop: id});
            return res.send({...shop._doc, images: images, reviews: reviews});
        } catch (error) {
            return res.status(400).send(error);
        }
    }
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const id = req.params.id;
    try {
        const shop = await Shop.findOne({_id: id});
        if (!shop) {
            return res.status(404).send("No this shop");
        }
        await fs.promises.unlink(path.join(config.uploadPath, shop.image));
        await Review.deleteMany({shop: shop});
        const images = await Image.find({shop: shop});
        images.map(async image => {
            await fs.promises.unlink(path.join(config.uploadPath, image.url));
        });
        await Image.deleteMany({shop: shop});
        await Shop.deleteOne({_id: id});
        res.send("ok");
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;