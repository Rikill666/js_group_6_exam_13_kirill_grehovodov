const express = require('express');
const ValidationError = require('mongoose').Error.ValidationError;
const auth = require('../middleware/auth');
const Review = require('../models/Review');
const Shop = require('../models/Shop');
const permit = require('../middleware/permit');
const router = express.Router();

router.get('/:id', auth, async (req, res) => {
    const id = req.params.id;
    const user = req.user;
    const review = await Review.findOne({shop: id, user:user});
    if(!review){
        return res.send(null);
    }
    return res.send(review);
});

router.post('/', auth, async (req, res) => {
    if (!req.body.comment) {
        return res.status(400).send({errors: {comment: {message: "You have not added a comment"}}});
    }
    if (!req.body.kitchenQuality){
        return res.status(400).send({errors: {kitchenQuality: {message: "You did not give a grade for the kitchen"}}});
    }
    if (!req.body.serviceQuality){
        return res.status(400).send({errors: {serviceQuality: {message: "You did not rate the service"}}});
    }
    if (!req.body.interiorQuality){
        return res.status(400).send({errors: {interiorQuality: {message: "You did not give a grade for the interior"}}});
    }
    //сделать проверку на то что пользователь уже оставлял отзыв этому заведению
    try {
        const user = req.user;
        const shop = await Shop.findOne({_id: req.body.shop});
        if (!shop) {
            return res.status(404).send({message: "Shop not found"});
        }
        const old_review = await Review.findOne({shop: shop, user:user});
        if(old_review){
            return res.status(400).send({message: "You have already left a review for this place"});
        }
        const reviewData = {
            shop: shop,
            user: user,
            comment: req.body.comment,
            kitchenQuality: req.body.kitchenQuality,
            serviceQuality: req.body.serviceQuality,
            interiorQuality: req.body.interiorQuality,
        };

        const review = new Review(reviewData);
        await review.save();
        await shop.recalculationOfRatings();
        return res.send(review);
    } catch (e) {
        if (e instanceof ValidationError) {
            return res.status(400).send(e);
        } else {
            console.log(e);
            return res.sendStatus(500);
        }
    }
});

router.delete('/:id', [auth, permit('admin', 'user')], async (req, res) => {
    try {
        const id = req.params.id;
        const review = await Review.findOne({_id: id});
        if (!review) {
            return res.status(404).send("No this review");
        }
        if(review.user.toString() !== req.user._id.toString() && req.user.role !== "admin"){
            return res.status(403).send({'message': 'Unauthorized'});
        }
        const shop = await Shop.findOne({_id: review.shop});
        if (!shop) {
            return res.status(404).send({message: "Shop not found"});
        }
        await Review.deleteOne({_id: id});
        await shop.recalculationOfRatings();
        res.send("ok");
    } catch (error) {
        return res.status(400).send(error);
    }
});
module.exports = router;