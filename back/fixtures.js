const mongoose = require('mongoose');
const config = require('./config');
const Shop = require('./models/Shop');
const User = require('./models/User');
const Image = require('./models/Image');
const Review = require('./models/Review');

const {nanoid} = require("nanoid");

const run = async () => {
    await mongoose.connect(config.database, config.databaseOptions);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (let coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [user, user2, admin] = await User.create({
        username: 'user',
        password: '123',
        token: nanoid(),
        displayName: "User"
    },{
        username: 'user2',
        password: '123',
        token: nanoid(),
        displayName: "User2"
    }, {
        username: 'admin',
        password: '123',
        role: 'admin',
        token: nanoid(),
        displayName: "Admin"
    });


    const [one, two, three, four, five] = await Shop.create({
            user: user,
            title: "Sunset",
            description:"vauvau1",
            image: 'uploads/fixtures/sunset.jpg'
        }, {
            user: user,
            title: "Winter",
            description:"vauvau2",
            image: 'uploads/fixtures/winter.jpg',
        }, {
            user: user,
            description:"vauvau3",
            title: "Sea",
            image: 'uploads/fixtures/sea.jpg'
        },
        {
            user: user2,
            description:"vauvau4",
            title: "Spring",
            image: 'uploads/fixtures/spring.jpg'
        },
        {
            user: user2,
            description:"vauvau5",
            title: "Sahara",
            image: 'uploads/fixtures/sahara.jpg'
        }
    );
    await Image.create({
        user:user,
        shop: one,
        url:'uploads/fixtures/spring.jpg'
    },{
        user:user2,
        shop: one,
        url:'uploads/fixtures/spring.jpg'
    },{
        user:admin,
        shop: one,
        url:'uploads/fixtures/spring.jpg'
    },{
        user:user2,
        shop: two,
        url:'uploads/fixtures/spring.jpg'
    },{
        user:user,
        shop: one,
        url:'uploads/fixtures/spring.jpg'
    },{
        user:user,
        shop: two,
        url:'uploads/fixtures/sahara.jpg'
    },);
    await Review.create({
        user:user,
        shop: one,
        comment:"blabla",
        kitchenQuality: 4,
        serviceQuality: 3,
        interiorQuality: 5
    },{
        user:user,
        shop: two,
        comment:"blabla",
        kitchenQuality: 5,
        serviceQuality: 5,
        interiorQuality: 5
    },{
        user:user2,
        shop: two,
        comment:"blabla",
        kitchenQuality: 3,
        serviceQuality: 3,
        interiorQuality: 4
    },{
        user:admin,
        shop: two,
        comment:"blabla",
        kitchenQuality: 3,
        serviceQuality: 1,
        interiorQuality: 5
    },);

    await one.recalculationOfRatings();
    await two.recalculationOfRatings();
    mongoose.connection.close();
};

run().catch(e => {
    mongoose.connection.close();
    throw e;
});