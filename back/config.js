const path = require('path');

const rootPath = __dirname;

const env = process.env.NODE_ENV;

let database = 'mongodb://localhost/shops_ratings';
let port = 8000;

if (env === 'test') {
  database = 'mongodb://localhost/shops_ratings_test';
  port = 8010;
}

module.exports = {
  port,
  rootPath,
  uploadPath: path.join(rootPath, 'public'),
  database,
  databaseOptions: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  },
  facebook: {
    appId: '622346031722687',
    appSecret: '67f8b3de74e159551b5048b25386c915'
  }
};