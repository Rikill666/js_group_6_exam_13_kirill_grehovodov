const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ImageSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    shop: {
        type: Schema.Types.ObjectId,
        ref: 'Shop',
        required: true
    },
    createDate:{
        type: Date,
        default: new Date
    },
    url: {
        type: String,
        required: true
    },
});

const Image = mongoose.model('Image', ImageSchema);

module.exports = Image;