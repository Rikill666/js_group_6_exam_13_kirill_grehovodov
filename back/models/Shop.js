const mongoose = require('mongoose');
const Review = require('./Review');
const Schema = mongoose.Schema;

const ShopSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    createDate:{
        type: Date,
        default: new Date
    },
    image: {
        type: String,
        required: true
    },
    averageRating: {
        type: Number,
        default: 0,
        min: 0,
        max: 5
    },
    averageKitchenQuality: {
        type: Number,
        default: 0,
        min: 0,
        max: 5
    },
    averageServiceQuality: {
        type: Number,
        default: 0,
        min: 0,
        max: 5
    },
    averageInteriorQuality: {
        type: Number,
        default: 0,
        min: 0,
        max: 5
    },
});

ShopSchema.methods.recalculationOfRatings = async function () {
    const resultData = await Review.find({shop: this._id});
    if(resultData.length > 0) {
        let result = resultData.reduce((acc, review) => {
                acc['count']++;
                acc['kitchenTotal'] += review.kitchenQuality;
                acc['serviceTotal'] += review.serviceQuality;
                acc['interiorTotal'] += review.interiorQuality;
                return acc;
            },
            {'count': 0, 'kitchenTotal': 0, 'serviceTotal': 0, 'interiorTotal': 0});
        this.averageKitchenQuality = parseFloat((result.kitchenTotal / result.count).toFixed(1));
        this.averageServiceQuality = parseFloat((result.serviceTotal / result.count).toFixed(1));
        this.averageInteriorQuality = parseFloat((result.interiorTotal / result.count).toFixed(1));
        this.averageRating = parseFloat(((this.averageKitchenQuality + this.averageServiceQuality + this.averageInteriorQuality) / 3).toFixed(1));
    }
    else{
        this.averageKitchenQuality = 0;
        this.averageServiceQuality = 0;
        this.averageInteriorQuality = 0;
        this.averageRating = 0;
    }
    await this.save();
};

const Shop = mongoose.model('Shop', ShopSchema);

module.exports = Shop;