const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    shop: {
        type: Schema.Types.ObjectId,
        ref: 'Shop',
        required: true
    },
    comment: {
        type: String,
        required: true
    },
    kitchenQuality:{
        type: Number,
        default: 0,
        min: 0,
        max: 5,
        required: true
    },
    serviceQuality:{
        type: Number,
        default: 0,
        min: 0,
        max: 5,
        required: true
    },
    createDate:{
        type: Date,
        default: new Date
    },
    interiorQuality:{
        type: Number,
        default: 0,
        min: 0,
        max: 5,
        required: true
    },
});

const Review = mongoose.model('Review', ReviewSchema);

module.exports = Review;