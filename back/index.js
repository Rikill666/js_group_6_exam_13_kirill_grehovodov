const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');
const shops = require('./app/shops');
const users = require('./app/users');
const reviews = require('./app/reviews');
const images = require('./app/images');

const app = express();

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const run = async () => {
    await mongoose.connect(config.database, config.databaseOptions);

    app.use('/shops', shops);
    app.use('/users', users);
    app.use('/reviews', reviews);
    app.use('/images', images);

    app.listen(config.port, () => {
        console.log(`HTTP Server started on ${config.port} port!`);
    });
};

run().catch(e => {
    console.error(e);
});